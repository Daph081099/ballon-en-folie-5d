export class Bouton extends createjs.Bitmap{
    constructor(atlas){
        super(atlas);

        this.alpha = 0;
    }

    apparaitre(){

        console.log('Bouton apparait');
        createjs.Tween
            .get(this)
            .to({alpha:1}, 1500, createjs.Ease.linear);
    }

    enlever(){
        createjs.Tween
            .get(this)
            .to({alpha:0}, 1500, createjs.Ease.linear)
    }
}