import {Bouton} from "./Bouton.js";
import {ArcadeInterface} from "./ArcadeInterface.js";
import {OceanInterface} from "./OceanInterface.js";
import {VolcanInterface} from "./VolcanInterface.js";
import {JungleInterface} from "./JungleInterface.js";
import {EspaceInterface} from "./EspaceInterface.js";

export let ballon, canvas;
export let tempsToucher = 0, ballonPosXinit, ballonPosYinit, ballonPosXfinal, ballonPosYfinal, peutLancer = true;


export class Interface {

    constructor() {


        document.getElementById('code').focus();

        let entreeUtilisateur;
        let nomUtilisateur;

        document.querySelector('button').addEventListener('click', () => {

            entreeUtilisateur = document.getElementById('code').value;
            nomUtilisateur = document.getElementById('pseudo').value;
            if (entreeUtilisateur === "") {
                alert('Veuillez saisir le code');
            } else if (nomUtilisateur === "") {
                alert('Veuillez saisir un pseudo');
            } else {
                this.connexion(entreeUtilisateur, nomUtilisateur);
            }
        })
    }

    connexion(data, dataNom) {

        // Instantiation du client Socket.IO pour communiquer avec le serveur
        this.socket = io("https://dstonge.dectim.ca:3002", {
            query: {type: "interface", code: data, name: dataNom}
        });

        this.socket.on('erreur', () => alert('mauvais code'));

        this.socket.on('succes', () => {

            //Enlever le form
            document.querySelector('.form').style.display = 'none';

            // Envoi d'un événement "click" quand le joueur clique sur la page
            document.body.addEventListener("click", e => {
                this.socket.emit("click", {type: "click", x: e.clientX, y: e.clientY});
            });

            // Envoi d'un événement "touchmove" quand le joueur touche à la page
            document.body.addEventListener("touchmove", e => {
                this.socket.emit("touchmove", {type: "touchmove", x: e.touches[0].clientX, y: e.touches[0].clientY});
            });

            this.stage = null;

            canvas = document.querySelector('canvas');

            canvas.width = window.innerWidth;
            canvas.height = window.innerHeight;

            canvas.style.backgroundColor = '#01002C';

            this.parametres = {
                manifeste: "ressources/manifest.json"
            };

            this.charger();

            this.timer = null;


            this.socket.on('redemarrer', ()=>{

                this.partirJeu();
            })
        });


    }

    charger() {

        this.chargeur = new createjs.LoadQueue();
        this.chargeur.installPlugin(createjs.Sound);
        this.chargeur.addEventListener("complete", this.creerStage.bind(this));
        this.chargeur.addEventListener('error', this.abandonner.bind(this));
        this.chargeur.loadManifest(this.parametres.manifeste);

    }

    abandonner(e) {
        alert("L'élément suivant n'a pu être chargé: " + e.src);
    }


    creerStage() {


        this.stage = new createjs.StageGL(canvas, {'antialias': true});
        this.stage.setClearColor('#01002C');

        createjs.Ticker.addEventListener('tick', e => this.stage.update(e));
        createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
        createjs.Ticker.framerate = 60;

        this.ajouterBouton();
    }

    ajouterBouton() {

        this.bouton1 = new Bouton(this.chargeur.getResult("boutonlancerJeu"));
        this.stage.addChild(this.bouton1);

        this.bouton1.apparaitre();

        this.bouton1.regX = this.bouton1.getBounds().width / 2;
        this.bouton1.regY = this.bouton1.getBounds().height / 2;

        this.bouton1.x = canvas.width / 2;
        this.bouton1.y = canvas.height / 2 - 100;

        this.bouton2 = new Bouton(this.chargeur.getResult("boutonInstruction"));
        this.stage.addChild(this.bouton2);

        this.bouton2.apparaitre();

        this.bouton2.regX = this.bouton2.getBounds().width / 2;
        this.bouton2.regY = this.bouton2.getBounds().height / 2;

        this.bouton2.x = canvas.width / 2;
        this.bouton2.y = canvas.height / 2 + 50;

        this.bouton2.addEventListener('click', this.ouvrirInstructions.bind(this));
        this.bouton1.addEventListener('click', this.partirJeu.bind(this));

    }

    ouvrirInstructions() {
        // Connecter les instructions pour qu'elle ouvre dans l'interafce de jeu

        createjs.Sound.play('sonBouton');

        this.stage.removeAllChildren();

        this.socket.emit('instructions');

        this.bouton1.enlever();
        this.bouton2.enlever();

        // bouton suivant
        this.bouton3 = new Bouton(this.chargeur.getResult("boutonSuivant"));
        this.stage.addChild(this.bouton3);

        this.bouton3.apparaitre();

        this.bouton3.regX = this.bouton3.getBounds().width / 2;
        this.bouton3.regY = this.bouton3.getBounds().height / 2;

        this.bouton3.x = canvas.width / 2;
        this.bouton3.y = canvas.height / 2 + 50;

        this.bouton3.addEventListener('click', this.ouvrirInstructions2.bind(this));
    }

    ouvrirInstructions2() {
        createjs.Sound.play('sonBouton');

        this.socket.emit('instructions2');
        this.stage.removeAllChildren();


        // manque video
        //Bouton precedent

        this.bouton4 = new Bouton(this.chargeur.getResult("boutonPrecedent"));
        this.stage.addChild(this.bouton4);

        this.bouton4.apparaitre();

        this.bouton4.regX = this.bouton4.getBounds().width / 2;
        this.bouton4.regY = this.bouton4.getBounds().height / 2;

        this.bouton4.x = canvas.width / 2;
        this.bouton4.y = canvas.height / 2 - 100;

        this.bouton4.addEventListener('click', this.ouvrirInstructions.bind(this));

        //Bon jouer

        this.stage.addChild(this.bouton1);
        this.bouton1.apparaitre();
        this.bouton1.y = canvas.height / 2 + 50;

        this.bouton1.addEventListener('click', this.partirJeu.bind(this));
    }


    partirJeu() {



        this.socket.emit('jeu');

        this.stage.removeAllChildren();

        // Bouton environnement

        // Arcade
        this.boutonArcade = new Bouton(this.chargeur.getResult("boutonArcade"));
        this.stage.addChild(this.boutonArcade);
        this.boutonArcade.apparaitre();
        this.boutonArcade.regX = this.boutonArcade.getBounds().width / 2;
        this.boutonArcade.regY = this.boutonArcade.getBounds().height / 2;
        this.boutonArcade.x = canvas.width / 2;
        this.boutonArcade.y = 100;
        this.boutonArcade.scale = 0.9;
        this.boutonArcade.addEventListener('click', this.partirArcade.bind(this));

        // Ocean
        this.boutonOcean = new Bouton(this.chargeur.getResult("boutonOcean"));
        this.stage.addChild(this.boutonOcean);
        this.boutonOcean.apparaitre();
        this.boutonOcean.regX = this.boutonOcean.getBounds().width / 2;
        this.boutonOcean.regY = this.boutonOcean.getBounds().height / 2;
        this.boutonOcean.x = canvas.width / 2;
        this.boutonOcean.y = this.boutonArcade.y + 100;
        this.boutonOcean.scale = 0.9;
        this.boutonOcean.addEventListener('click', this.partirOcean.bind(this));

        // Volcan
        this.boutonVolcan = new Bouton(this.chargeur.getResult("boutonVolcan"));
        this.stage.addChild(this.boutonVolcan);
        this.boutonVolcan.apparaitre();
        this.boutonVolcan.regX = this.boutonVolcan.getBounds().width / 2;
        this.boutonVolcan.regY = this.boutonVolcan.getBounds().height / 2;
        this.boutonVolcan.x = canvas.width / 2;
        this.boutonVolcan.y = this.boutonOcean.y + 100;
        this.boutonVolcan.scale = 0.9;
        this.boutonVolcan.addEventListener('click', this.partirVolcan.bind(this));

        // Jungle
        this.boutonJungle = new Bouton(this.chargeur.getResult("boutonJungle"));
        this.stage.addChild(this.boutonJungle);
        this.boutonJungle.apparaitre();
        this.boutonJungle.regX = this.boutonJungle.getBounds().width / 2;
        this.boutonJungle.regY = this.boutonJungle.getBounds().height / 2;
        this.boutonJungle.x = canvas.width / 2;
        this.boutonJungle.y = this.boutonVolcan.y + 100;
        this.boutonJungle.scale = 0.9;
        this.boutonJungle.addEventListener('click', this.partirJungle.bind(this));

        // Espace
        this.boutonEspace = new Bouton(this.chargeur.getResult("boutonEspace"));
        this.stage.addChild(this.boutonEspace);
        this.boutonEspace.apparaitre();
        this.boutonEspace.regX = this.boutonEspace.getBounds().width / 2;
        this.boutonEspace.regY = this.boutonEspace.getBounds().height / 2;
        this.boutonEspace.x = canvas.width / 2;
        this.boutonEspace.y = this.boutonJungle.y + 100;
        this.boutonEspace.scale = 0.9;
        this.boutonEspace.addEventListener('click', this.partirEspace.bind(this));
    }

    partirArcade() {
        createjs.Sound.play('sonBouton');

        this.socket.emit('arcade');
        this.stage.removeAllChildren();

        this.interface = new ArcadeInterface(this.socket, this.stage, this.chargeur);

    }

    partirOcean() {
        createjs.Sound.play('sonBouton');

        this.socket.emit('ocean');
        this.stage.removeAllChildren();

        this.interface = new OceanInterface(this.socket, this.stage, this.chargeur);
    }

    partirVolcan() {
        createjs.Sound.play('sonBouton');

        this.socket.emit('volcan');
        this.stage.removeAllChildren();

        this.interface = new VolcanInterface(this.socket, this.stage, this.chargeur);

    }

    partirJungle() {
        createjs.Sound.play('sonBouton');

        this.socket.emit('jungle');
        this.stage.removeAllChildren();

        this.interface = new JungleInterface(this.socket, this.stage, this.chargeur);
    }

    partirEspace() {
        createjs.Sound.play('sonBouton');

        this.socket.emit('espace');
        this.stage.removeAllChildren();

        this.interface = new EspaceInterface(this.socket, this.stage, this.chargeur);
    }


}
