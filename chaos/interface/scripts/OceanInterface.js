import {Bouton} from "./Bouton.js";

export let ballon,plateforme, canvas;
export let tempsToucher = 0, ballonPosXinit, ballonPosYinit, ballonPosXfinal, ballonPosYfinal, peutLancer = true;

let temps = 60;

export class OceanInterface {
    constructor(socket, stage, chargeur) {

        this.socket = socket;
        this.stage = stage;
        this.chargeur = chargeur;

        this.ecouteur = {
            ecouteurMouvement: this.gererMouvement.bind(this),
            ecouteurFinMouvement: this.gererFinMouvement.bind(this),
            ecouteurDebutMouvement: this.gererDebutMouvement.bind(this),

            ecouteurMenuOverlay: this.apparaitreMenuOverlay.bind(this),
            ecouteurStage: this.updateStage.bind(this),
            ecouteurDetruire: this.detruire.bind(this),

            ecouteurFermerMenu: this.fermerMenu.bind(this),
            ecouteurToggleSon: this.toggleSon.bind(this)
        };


        // Envoi d'un événement "click" quand le joueur clique sur la page
        document.body.addEventListener("click", e => {
            this.socket.emit("click", {type: "click", x: e.clientX, y: e.clientY});
        });

        document.querySelector('.menu-overlay').style.display = 'none';

        this.setting = document.querySelector('.interface-settings');
        this.setting.style.display = 'flex';
        let sortirJeu = document.querySelector('.bouton-sortir');


        this.setting.addEventListener('click', this.ecouteur.ecouteurMenuOverlay);
        document.body.addEventListener("touchmove", this.ecouteur.ecouteurMouvement);
        document.body.addEventListener("touchend", this.ecouteur.ecouteurFinMouvement);
        document.body.addEventListener("touchstart", this.ecouteur.ecouteurDebutMouvement);
        document.querySelector('.bouton-fermer').addEventListener('click',this.ecouteur.ecouteurFermerMenu);
        sortirJeu.addEventListener('click', this.ecouteur.ecouteurDetruire);

        let muteSon = document.querySelector('.bouton-son');
        muteSon.addEventListener('click', this.ecouteur.ecouteurToggleSon);

        canvas = document.querySelector('canvas');

        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;

        canvas.style.backgroundColor = '#01002C';

        this.parametres = {
            manifeste: "ressources/manifest.json"
        };

        this.updateStage();
        this.ajouterTemps();

        this.timer = null;
    }

    updateStage() {

        this.ajouterPlateforme();
        this.ajouterBallon();
    }

    fermerMenu(){
        this.socket.emit('overlayClose');

        document.querySelector('.menu-overlay').style.display = 'none';

        document.body.addEventListener('touchmove', this.ecouteur.ecouteurMouvement);
        document.body.addEventListener('touchend', this.ecouteur.ecouteurFinMouvement);
        document.body.addEventListener('touchstart', this.ecouteur.ecouteurDebutMouvement);


        this.ajouterInterval();
    }

    toggleSon(){
        this.socket.emit('son');
        let imageSon = document.querySelector('.image-son');
        if (imageSon.getAttribute('src') === './ressources/images/ic_round-volume-on.png') {
            imageSon.setAttribute('src', './ressources/images/ic_round-volume-off.png')
        } else {
            imageSon.setAttribute('src', './ressources/images/ic_round-volume-on.png')
        }
    }

    detruire() {
        temps = 60;
        $('#timer').text(temps);


        this.socket.emit('quitterOcean');

        clearInterval(this.interval);
        this.interval =null;

        this.stage.removeAllChildren();

        document.getElementById('timer').style.display = 'none';

        this.setting.style.display = 'none';

        document.querySelector('.menu-overlay').style.display = 'none';

        this.setting.removeEventListener('click', this.ecouteur.ecouteurMenuOverlay);
        document.querySelector('.bouton-sortir').removeEventListener('click', this.ecouteur.ecouteurDetruire);
        document.body.removeEventListener("touchmove", this.ecouteur.ecouteurMouvement);
        document.body.removeEventListener("touchend", this.ecouteur.ecouteurFinMouvement);
        document.body.removeEventListener("touchstart", this.ecouteur.ecouteurDebutMouvement);

        document.querySelector('.bouton-fermer').removeEventListener('click',this.ecouteur.ecouteurFermerMenu);
        document.querySelector('.bouton-son').removeEventListener('click', this.ecouteur.ecouteurToggleSon);

        this.stage = null;

    }


    ajouterTemps() {



        document.getElementById('timer').style.display = 'block';

        temps = 60;

        setTimeout(this.ajouterInterval.bind(this), 2000);

    }

    ajouterInterval() {
        this.interval = setInterval(() => {
            temps--;
            $('#timer').text(temps);

            if (temps < 10) {
                document.getElementById('timer').style.color = '#EE3A00';
            } else {
                document.getElementById('timer').style.color = '#FFF'
            }


            if (temps === 0) {
                clearInterval(this.interval);
                document.getElementById('timer').style.display = 'none';

                this.stage.removeChild(ballon, plateforme);

                setTimeout(this.boutonRecommencer(),5000);

            }
        }, 1000);

    }

    boutonRecommencer(){

        this.recommencer = new Bouton(this.chargeur.getResult("recommencer"));
        this.stage.addChild(this.recommencer);
        this.recommencer.apparaitre();
        this.recommencer.regX = this.recommencer.getBounds().width / 2;
        this.recommencer.regY = this.recommencer.getBounds().height / 2;
        this.recommencer.x = canvas.width / 2;
        this.recommencer.y = canvas.height / 2;

        this.recommencer.addEventListener('click', this.ecouteur.ecouteurDetruire);


    }

    gererMouvement(e) {



        this.socket.emit('message', 'gererMouvement')

        if (peutLancer) {
            this.socket.emit('message', 'gererMv -> peutLancer')

            if (e.touches[0].clientY < canvas.height / 2) {
                if (e.touches[0].clientX < canvas.width / 2) {
                    this.socket.emit("posElement", {
                        x: -((canvas.width / 2) - e.touches[0].clientX) * 5,
                        y: -((canvas.height / 2) - e.touches[0].clientY) * 2
                    });
                } else if (e.touches[0].clientX > canvas.width / 2) {
                    this.socket.emit("posElement", {
                        x: (e.touches[0].clientX - canvas.width / 2) * 5,
                        y: -((canvas.height / 2) - e.touches[0].clientY) * 2
                    });
                }
            } else if (e.touches[0].clientY > canvas.height / 2) {
                if (e.touches[0].clientX < canvas.width / 2) {
                    this.socket.emit("posElement", {
                        x: -((canvas.width / 2) - e.touches[0].clientX) * 5,
                        y: (e.touches[0].clientY - canvas.height / 2) * 2
                    });
                } else if (e.touches[0].clientX > canvas.width / 2) {
                    this.socket.emit("posElement", {
                        x: (e.touches[0].clientX - canvas.width / 2) * 5,
                        y: (e.touches[0].clientY - canvas.height / 2) * 2
                    });
                }
            }

            ballon.x = e.touches[0].clientX;
            ballon.y = e.touches[0].clientY;

        }
    }

    gererDebutMouvement() {

        if (peutLancer) {

            ballonPosXinit = ballon.x;
            ballonPosYinit = ballon.y;

            this.timer = setInterval(() => {
                tempsToucher += 1
            }, 1);

        }
    }

    gererFinMouvement() {

        if (peutLancer) {
            peutLancer = false;

            clearInterval(this.timer);

            ballonPosXfinal = ballon.x;
            ballonPosYfinal = ballon.y;

            this.lancerBallonInterfaceMobile(tempsToucher, ballonPosXinit, ballonPosYinit, ballonPosXfinal, ballonPosYfinal);
            this.socket.emit('lancer', {
                temps: tempsToucher,
                posXinit: ballonPosXinit,
                posYinit: ballonPosYinit,
                posXfinal: ballonPosXfinal,
                posYfinal: ballonPosYfinal
            });

            tempsToucher = 0;

            setTimeout(() => {
                ballon.x = canvas.width / 2
                ballon.y = canvas.height - 120;
                peutLancer = true;

            }, 5000);
        }
    }


    ajouterBallon() {
        ballon = new createjs.Bitmap(this.chargeur.getResult('ballon-ocean'));
        this.stage.addChild(ballon);

        ballon.regX = ballon.getBounds().width / 2;
        ballon.regY = ballon.getBounds().height / 2;

        ballon.x = canvas.width / 2;
        ballon.y = canvas.height - 120;

        ballon.scale = 0.1;

    }

    ajouterPlateforme() {
        plateforme = new createjs.Bitmap(this.chargeur.getResult('plateforme-ocean'));
        this.stage.addChild(plateforme);

        plateforme.regX = plateforme.getBounds().width / 2;
        plateforme.regY = plateforme.getBounds().height / 2;

        plateforme.width = window.innerWidth;

        plateforme.x = canvas.width / 2;
        plateforme.y = canvas.height - 120;
        plateforme.scale = 0.85;

    }

    lancerBallonInterfaceMobile(tempsToucherVelo, pXinit, pYinit, pXfinal, pYfinal) {

        let distanceY = pYinit - pYfinal;


        if (distanceY < 0) {

        } else {
            let velocite = distanceY / tempsToucherVelo;


            createjs.Tween.get(ballon)
                .to({y: -200},)

        }


    }

    apparaitreMenuOverlay() {

        let menu = document.querySelector('.menu-overlay');


        if (menu.style.display === 'none') {
            this.socket.emit('overlay');
            menu.style.display = 'flex';

            document.body.removeEventListener('touchmove', this.ecouteur.ecouteurMouvement);
            document.body.removeEventListener('touchend', this.ecouteur.ecouteurFinMouvement);
            document.body.removeEventListener('touchstart', this.ecouteur.ecouteurDebutMouvement);

            clearInterval(this.interval);
            this.interval = null;


        } else if (menu.style.display === 'flex') {
            this.socket.emit('overlayClose');
            menu.style.display = 'none';

            document.body.addEventListener('touchmove', this.ecouteur.ecouteurMouvement);
            document.body.addEventListener('touchend', this.ecouteur.ecouteurFinMouvement);
            document.body.addEventListener('touchstart', this.ecouteur.ecouteurDebutMouvement);


            this.ajouterInterval();

            peutLancer = true;

        }


    }
}

