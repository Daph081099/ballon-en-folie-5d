<!doctype html>
<?php include_once('bd_includes/bd_functions.php') ?>
<html lang="fr">

<head>

    <meta charset="UTF-8">
    <title>Ballon en folie : 5D</title>


    <link href="https://fonts.googleapis.com/css2?family=Fugaz+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Baloo+Tammudu+2&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="styles/styles.css">
    <script src="bibliotheques/socket.io.js" defer></script>
    <script src="bibliotheques/createjs.js" defer></script>
    <script src="bibliotheques/three.js" defer type="module"></script>
    <script src="bibliotheques/three.module.js" defer></script>
    <script src="bibliotheques/physi.js" defer></script>
    <script src="bibliotheques/physijs_worker.js" defer></script>
    <script src="bibliotheques/tween.js" defer></script>
    <script src="bibliotheques/stats.js" defer></script>

    <script src="bibliotheques/js/Three.js"></script>
    <script src="bibliotheques/js/Detector.js"></script>
    <script src="bibliotheques/js/Stats.js"></script>
    <script src="bibliotheques/js/OrbitControls.js"></script>
    <script src="bibliotheques/js/THREEx.KeyboardState.js"></script>
    <script src="bibliotheques/js/THREEx.WindowResize.js"></script>

    <!-- jQuery code to display an information button and box when clicked. -->
    <script src="bibliotheques/js/jquery-1.9.1.js"></script>
    <script src="bibliotheques/js/jquery-ui.js"></script>
    <link rel=stylesheet href="styles/jquery-ui.css"/>

    <script type="module">
        import {Debut} from "./scripts/Debut.js";

        new Debut();
    </script>


</head>

<body>

<!--Animation de debut-->

<video autoplay class="intro">
    <source src="./ressources/video/INTRO.mp4" type="video/mp4"/>
</video>

<!--Tableau de pointage-->

<div class="conteneur-highscore" id="conteneur-arcade">
    <div class="conteneur-relative">
        <img src="./ressources/images/ScoreArcade.png" alt="Tableau score arcade">
        <h1 class="highscore-arcade">score: 0</h1>
    </div>
</div>

<div class="conteneur-highscore" id="conteneur-ocean">
    <div class="conteneur-relative">
        <img src="./ressources/images/ScoreOcean.png" alt="Tableau score ocean">
        <h1 class="highscore-ocean">score: 0</h1>
    </div>
</div>

<div class="conteneur-highscore" id="conteneur-volcan">
    <div class="conteneur-relative">
        <img src="./ressources/images/ScoreVolcan.png" alt="Tableau score volcan">
        <h1 class="highscore-volcan">score: 0</h1>
    </div>
</div>

<div class="conteneur-highscore" id="conteneur-espace">
    <div class="conteneur-relative">
        <img src="./ressources/images/ScoreEspace.png" alt="Tableau score espace">
        <h1 class="highscore-espace">score: 0</h1>
    </div>
</div>

<div class="conteneur-highscore" id="conteneur-jungle">
    <div class="conteneur-relative">
        <img src="./ressources/images/ScoreJungle.png" alt="Tableau score jungle">
        <h1 class="highscore-jungle">score: 0</h1>
    </div>
</div>


<div class="conteneur-pointage-fin">
    <div class="conteneur-relative">
        <img src="./ressources/images/findepartie.png" alt="image de tableau de fin de partie">
        <div class="fin-absolute">
            <h1 class="pointage-typo">Fin de la partie</h1>
            <h1 class="pointage-typo pointage-final">Votre pointage: 0</h1>
        </div>

    </div>
</div>

<video style="display: none" autoplay loop class="video-environnement">
    <source class="video" src="./ressources/video/ARCADE.mp4" type="video/mp4"/>
</video>


<div class="div-code">
    <p class="qr">Scannez le code QR à partir de votre téléphone par la caméra ou une application mobile (exemple : Google Lens)</p>
    <p class="code">Entrez le code dans votre téléphone </p>
</div>

<div id="timer">60</div>

<div class="credit">
    <p class="credit-texte">Crédit</p>
    <div class="conteneur-credit">
        <img class="credit-image" src="./ressources/images/credit.png" alt="Page de credit">
        <img class="fermer" src="./ressources/images/Vector.png" alt="fermer">
    </div>
</div>

<canvas id="canvas-jeu">
</canvas>

</body>

</html>
