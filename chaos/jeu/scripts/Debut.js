import {Titre} from "./Titre.js";
import {Instruction} from "./Instruction.js";
import {Arcade} from "./Arcade.js";
import {Ocean} from "./Ocean.js";
import {Volcan} from "./Volcan.js";
import {Jungle} from "./Jungle.js";
import {Espace} from "./Espace.js";

let canvas, instruction1, instruction2, instruction3, codeAleatoire;

export class Debut {
    constructor() {

        //generer le code aleatoire ABC
        codeAleatoire = "";
        let possible = "ABCDEFGHJKMNPQRSTWXYZabcdeghjkmnpqrstwxyz123456789";
        for (let i = 0; i < 5; i++) {
            codeAleatoire += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        // Socket
        this.socket = io("https://dstonge.dectim.ca:3002", {
            query: {type: 'jeu', code: codeAleatoire} // identification en tant que jeu
        });

        this.stage = null;

        // Variable a revoir
        canvas = document.querySelector('#canvas-jeu');
        instruction1 = document.querySelector('.instruction1');
        instruction2 = document.querySelector('.instruction2');
        instruction3 = document.querySelector('.instruction3');

        // Canvas plein ercan
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;

        // Canvas couleur
        canvas.style.backgroundColor = '#01002C';

        // Manifeste
        this.parametres = {
            manifeste: "ressources/manifest.json"
        };


        this.charger();


        // Socket des boutons
        this.socket.on("afficherInstruction", () => {
            this.afficherInstruction();
        });

        this.socket.on('infoJoueur', nom => {
            this.nomJoueur = nom;
        });

        this.socket.on("partirJeu", () => {
            this.partirJeu();
        });

        this.socket.on("partirArcade", () => {
            this.partirArcade();
        });

        this.socket.on("partirOcean", () => {
            this.partirOcean();
        });

        this.socket.on("partirVolcan", () => {
            this.partirVolcan();
        });

        this.socket.on("partirJungle", () => {
            this.partirJungle();
        });

        this.socket.on("partirEspace", () => {
            this.partirEspace();
        });


    }


    charger() {

        this.chargeur = new createjs.LoadQueue();
        this.chargeur.installPlugin(createjs.Sound);
        this.chargeur.addEventListener("complete", this.creerStage.bind(this));
        this.chargeur.addEventListener('error', this.abandonner.bind(this));
        this.chargeur.loadManifest(this.parametres.manifeste);
    }

    abandonner(e) {
        alert("L'élément suivant n'a pu être chargé: " + e.src);
    }

    creerStage() {

        this.stage = new createjs.StageGL(canvas, {'antialias': true});
        this.stage.setClearColor('#01002C');

        createjs.Ticker.addEventListener('tick', e => this.stage.update(e));
        createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
        createjs.Ticker.framerate = 60;

        this.animationAccueil();

    }

    animationAccueil() {

        let videoIntro = document.querySelector('.intro');
        let videoEnd = false;

        videoIntro.addEventListener('ended', () => {
            videoIntro.style.display = 'none';
            videoEnd = true;
        });

        setTimeout(()=>{
            if(videoEnd){
                this.animationAccueil2();
            }
            else{
                document.querySelector('.intro').style.display = 'none';
                this.animationAccueil2();
            }

        },3000);

    }

    animationAccueil2() {
        this.titre = new Titre(this.chargeur.getResult("nom"));
        this.stage.addChild(this.titre);
        this.titre.x = canvas.width / 2 - this.titre.getBounds().width / 2;
        this.titre.y = 100;
        this.titre.apparaitre();
        this.titre.bouger();

        this.codeQR = new Titre(this.chargeur.getResult('codeQR'));
        this.stage.addChild(this.codeQR);
        this.codeQR.x = canvas.width / 2 - this.codeQR.getBounds().width / 2;
        this.codeQR.y = 450;
        this.codeQR.apparaitre();

        this.code = document.querySelector('.code');
        this.code.style.opacity = 1;
        this.code.style.transition = 'opacity 1.5s';

        this.qr = document.querySelector('.qr');
        this.qr.style.opacity = 1;
        this.qr.style.transition = 'opacity 1.5s';

        this.code.innerHTML = "Entrez le code <span>" + codeAleatoire + "</span> dans votre téléphone";
        document.querySelector('span').style.color = "#E76D28";

        this.credit = document.querySelector('.credit-texte');
        this.credit.style.opacity = 1;
        this.credit.style.transition = 'opacity 1.5s';


        this.credit.addEventListener('click', () =>{
            if(document.querySelector('.conteneur-credit').style.display = 'none'){
                document.querySelector('.conteneur-credit').style.display = 'flex';
            }
            else{
                document.querySelector('.conteneur-credit').style.display = 'none';
            }
        });

        this.fermer = document.querySelector('.fermer');
        this.fermer.addEventListener('click', () =>{
            if(document.querySelector('.conteneur-credit').style.display = 'flex'){
                document.querySelector('.conteneur-credit').style.display = 'none';
            }
        });


        // Sons
        this.musiqueAmbiance = createjs.Sound.play('musique-main', {loop: -1});
        this.musiqueAmbiance.volume = 0.2;
    }


    afficherInstruction() {
        this.titre.enlever();

        setTimeout(this.afficherInstruction1.bind(this), 2000);

        this.code.style.opacity = 0;
        this.qr.style.opacity = 0;
        this.qr.style.display = 'none';
        this.credit.style.opacity = 0;

        this.codeQR.enlever();

    }

    afficherInstruction1() {

        this.instruction = new Instruction(this.chargeur.getResult('instruction1'));
        this.stage.addChild(this.instruction);
        this.instruction.x = 400;
        this.instruction.y = 50;
        this.instruction.apparaitre();

        this.code.style.opacity = 0;
        this.qr.style.opacity = 0;
        this.qr.style.display = 'none';
        this.credit.style.opacity = 0;


        this.socket.on("afficherInstruction2", () => {

            this.afficherInstruction2();
        });
    }

    afficherInstruction2() {

        this.instruction.enlever();

        this.instruction2 = new Instruction(this.chargeur.getResult('instruction2'));
        this.stage.addChild(this.instruction2);
        this.instruction2.x = 400;
        this.instruction2.y = 50;
        this.instruction2.apparaitre();

        this.code.style.opacity = 0;
        this.qr.style.opacity = 0;
        this.qr.style.display = 'none';
        this.credit.style.opacity = 0;


    }


    partirJeu() {

        this.stage.removeAllChildren();

        // Tableau
        this.choix = new Instruction(this.chargeur.getResult('choix'));
        this.stage.addChild(this.choix);
        this.choix.x = 400;
        this.choix.y = 50;
        this.choix.apparaitre();

        setTimeout(this.environnementTableau.bind(this), 500);

        this.code.style.opacity = 0;
        this.qr.style.opacity = 0;
        this.credit.style.opacity = 0;

        this.codeQR.enlever();

    }

    environnementTableau() {

        canvas.style.display = 'block';
        // Arcade
        this.arcade = new Instruction(this.chargeur.getResult('arcade'));
        this.stage.addChild(this.arcade);
        this.arcade.x = 475;
        this.arcade.y = 300;
        this.arcade.apparaitre();

        // Ocean
        this.ocean = new Instruction(this.chargeur.getResult('ocean'));
        this.stage.addChild(this.ocean);
        this.ocean.x = 825;
        this.ocean.y = 300;
        this.ocean.apparaitre();


        // Volcan
        this.volcan = new Instruction(this.chargeur.getResult('volcan'));
        this.stage.addChild(this.volcan);
        this.volcan.x = 1175;
        this.volcan.y = 300;
        this.volcan.apparaitre();


        //Jungle
        this.jungle = new Instruction(this.chargeur.getResult('jungle'));
        this.stage.addChild(this.jungle);
        this.jungle.x = 625;
        this.jungle.y = 600;
        this.jungle.apparaitre();

        //Espace
        this.espace = new Instruction(this.chargeur.getResult('espace'));
        this.stage.addChild(this.espace);
        this.espace.x = 1025;
        this.espace.y = 600;
        this.espace.apparaitre();
    }

    partirArcade() {

        canvas.style.display = 'none';

        new Arcade(this.socket, this.nomJoueur);

        this.musiqueAmbiance.stop();
    }


    partirOcean() {

        canvas.style.display = 'none';

        new Ocean(this.socket, this.nomJoueur);

        this.musiqueAmbiance.stop();
    }

    partirVolcan() {

        canvas.style.display = 'none';

        new Volcan(this.socket, this.nomJoueur);

        this.musiqueAmbiance.stop();
    }

    partirJungle() {

        canvas.style.display = 'none';

        new Jungle(this.socket, this.nomJoueur);

        this.musiqueAmbiance.stop();
    }

    partirEspace() {

        canvas.style.display = 'none';

        new Espace(this.socket, this.nomJoueur);

        this.musiqueAmbiance.stop();
    }


}