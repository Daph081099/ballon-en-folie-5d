import {GLTFLoader} from "../bibliotheques/GLTFLoader.js";
import {panierVolcan} from "./Volcan.js";
import {panier} from "./Arcade.js";

export let scene, camera, renderer;
export let model, panierEspace;
export let ballonPris, ballonLancer, clientConnect, posBallonInitX, posBallonInitY, posBallonInitZ, gameover = false;


let plateformeBallon, colRimGauche, colRimDroite, colRimCentre, colPanneau;
let pointage = 0, peutCompter = true, peutCollisionObjet = true;
let physics_stats, render_stats, loader;
let timer = 60, interval;
let musiqueEspace, peutJouerSon = true;

export class Espace {

    constructor(socket, nom) {

        this.socket = socket;
        this.nomJoueur = nom;

        this.socket.on("joueur1", message => {
            this.bougerBallon(message);
        });

        this.socket.on("lancer", message => {
            this.lancerBallon(message.temps, message.posXinit, message.posYinit, message.posXfinal, message.posYfinal);
        });

        this.socket.on("quitterEspace", () => {
            this.detruire();
        });

        this.socket.on("overlay", () => {
            if (interval !== null) {
                clearInterval(interval);
                interval = null;
            }

        });

        this.socket.on('overlayClose', () => {
            if (interval === null) {
                this.ajouterInterval();
            }
        });

        Physijs.scripts.worker = './bibliotheques/physijs_worker.js';
        Physijs.scripts.ammo = 'ammo.js';

        this.initLoader();
        this.initThreeJS();

        this.ecouteur = {
            ecouteurTicker: this.detecterCollisionPoints.bind(this)
        };

        // Sons

        musiqueEspace = createjs.Sound.play('musique-espace', {loop: -1});
        musiqueEspace.volume = 0.5;

        createjs.Ticker.addEventListener('tick', this.ecouteur.ecouteurTicker);


        this.socket.on('son', () => {
            if (musiqueEspace.volume === 0.5) {
                musiqueEspace.volume = 0;
            } else {
                musiqueEspace.volume = 0.5;
            }
        });
    }

    initLoader() {
        loader = new GLTFLoader();

        loader.load('ressources/panierEspace.gltf', function (gltf) {

            panierEspace = gltf.scene;
            scene.add(panierEspace);

        }, undefined, function (error) {
            console.error(error);
        });

    }

    initThreeJS() {
        scene = new Physijs.Scene();
        scene.setGravity(new THREE.Vector3(0, -30, 0));

        camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 1000);

        renderer = new THREE.WebGLRenderer({antialias: true, alpha: true});
        renderer.setSize(window.innerWidth, window.innerHeight);
        renderer.shadowMap.enabled = true;
        renderer.shadowMapSoft = true;
        renderer.autoClearColor = false;
        document.body.appendChild(renderer.domElement);

        render_stats = new Stats();
        render_stats.domElement.style.position = 'absolute';
        render_stats.domElement.style.top = '0px';
        render_stats.domElement.style.zIndex = 100;
        document.body.appendChild(render_stats.domElement);

        physics_stats = new Stats();
        physics_stats.domElement.style.position = 'absolute';
        physics_stats.domElement.style.top = '50px';
        physics_stats.domElement.style.zIndex = 100;
        document.body.appendChild(physics_stats.domElement);


        this.ajouterVideo();
        this.ajouterElements();
        this.ajouterTimer();

    }

    ajouterVideo() {
        document.body.style.backgroundColor = 'transparent';

        this.source = document.querySelector('.video');
        this.video = document.querySelector('.video-environnement');


        this.video.pause();

        this.source.setAttribute('src', './ressources/video/GALAXY.mp4');

        this.video.load();


        this.video.style.display = 'block';
        this.video.style.position = 'absolute';
        this.video.style.zIndex = '-1';
        this.video.play();



    }

    ajouterTimer() {

        document.getElementById('timer').style.display = 'block';

        timer = 60;

        setTimeout(this.ajouterInterval.bind(this), 2000);

    }

    ajouterInterval() {

        interval = setInterval(() => {
            timer--;
            $('#timer').text(timer);

            if (timer < 10) {
                document.getElementById('timer').style.color = '#EE3A00';
            }
            else{
                document.getElementById('timer').style.color = '#FFF'
            }

            if (timer === 0) {
                clearInterval(interval);

                musiqueEspace.stop();

                document.querySelector('.conteneur-highscore').style.display = "none";

                this.afficherTableauFin();

                gameover = true;
                scene.remove(model);

            }
        }, 1000);
    }

    afficherTableauFin() {
        document.querySelector('.conteneur-pointage-fin').style.display = "flex";
        document.querySelector('.pointage-final').innerHTML = "Votre pointage : " + pointage;

        setTimeout(() => {
            document.querySelector('.conteneur-pointage-fin').style.display = "none";
        }, 5000);
    }


    ajouterElements() {
        pointage = 0;
        document.querySelector('.highscore-espace').innerHTML = this.nomJoueur+ " : " + pointage;


        //Tableau score
        document.querySelector('#conteneur-espace').style.display = "block";

        //Lumière ambiante
        let ambient = new THREE.AmbientLight(0x000000, 0.3);
        scene.add(ambient)
        let pointLight = new THREE.PointLight(0xffffff, 0.6, 200);
        pointLight.position.set(10, 10, 100);
        pointLight.castShadow = true;
        scene.add(pointLight);

        //Lumière directionnelle
        let dirLight = new THREE.DirectionalLight(0xffffff, 1, 100);
        dirLight.position.set(20, 15, 2);
        dirLight.castShadow = true;
        scene.add(dirLight);

        //Plancher        ----------------------------------------------//
        let material = new Physijs.createMaterial(new THREE.MeshBasicMaterial({
            color: 0x888888,
            opacity: 0,
            transparent: true
        }), 0, 1);

        //BOUNDS COLLISION ----------------------------------------------//
        colPanneau = new Physijs.BoxMesh(
            new THREE.BoxGeometry(13, 9, 0.3),
            material,
            0 // mass
        );
        colPanneau.position.z = -26;
        colPanneau.position.y = 10;
        scene.add(colPanneau);

        let colPoteau = new Physijs.BoxMesh(
            new THREE.BoxGeometry(1, 25, 1),
            material,
            0 // mass
        );
        colPoteau.position.z = -27;
        colPoteau.position.y = 0;
        scene.add(colPoteau);


        colRimGauche = new Physijs.BoxMesh(
            new THREE.BoxGeometry(0.2, 0.2, 8),
            material,
            0 // mass
        );

        colRimGauche.position.z = -26;
        colRimGauche.position.y = 5.5;
        colRimGauche.position.x = -3;
        scene.add(colRimGauche);

        colRimDroite = new Physijs.BoxMesh(
            new THREE.BoxGeometry(0.3, 0.3, 8),
            material,
            0 // mass
        );

        colRimDroite.position.z = -26;
        colRimDroite.position.y = 5.5;
        colRimDroite.position.x = 3;
        scene.add(colRimDroite);

        colRimCentre = new Physijs.BoxMesh(
            new THREE.BoxGeometry(6, 0.3, 0.3),
            material,
            0 // mass
        );

        colRimCentre.position.z = -21;
        colRimCentre.position.y = 5.5;

        scene.add(colRimCentre);

        //Plateforme ballon

        plateformeBallon = new Physijs.BoxMesh(
            new THREE.BoxGeometry(2, 0.3, 2),
            material,
            0 // mass
        );

        plateformeBallon.position.z = 0;
        plateformeBallon.position.y = -5;
        plateformeBallon.position.x = 0;
        scene.add(plateformeBallon);

        //Ballon
        let sphere_geometry = new THREE.SphereGeometry(2, 32, 32);

        let materialModel = new Physijs.createMaterial(new THREE.MeshBasicMaterial(
            {
                map: new THREE.TextureLoader().load('./ressources/images/textures/espace/moon_color.png'),
                bumpMap: THREE.ImageUtils.loadTexture('./ressources/images/textures/espace/moon_bump.png'),
                bumpScale: 0.6
            }), 0, 1);

        model = new Physijs.SphereMesh(sphere_geometry, materialModel, 10);
        model.position.set(0, 0, 0);
        model.setCcdMotionThreshold(1);
        model.setCcdSweptSphereRadius(0.2);

        model.addEventListener('collision', this.detecterCollisionObjets.bind(this));

        posBallonInitX = model.position.x;
        posBallonInitY = model.position.y;
        posBallonInitZ = model.position.z;

        scene.add(model);

        model.addEventListener('lancer', this.lancerBallon.bind(this));

        camera.position.z = 10;

        gameover = false;
        this.animate();
    }


    bougerBallon(message) {

        if (!gameover) {
            model.position.y = message.y / -180;
            model.position.x = message.x / 180;
        }


    }

    lancerBallon(temps, posXinit, posYinit, posXfinal, posYfinal) {

        if (!gameover) {

            let randNumb = Math.floor(Math.random() * 3) + 1;

            createjs.Sound.play('sonLancer' + randNumb);

            if (!ballonLancer) {
                ballonPris = true;
                ballonLancer = true;

                let tempsParcours = temps / 10;
                let distanceY = (posYinit - posYfinal) / 2;

                let velocite = (distanceY / tempsParcours) * 1.2;

                //Calculer l'angle du lancé
                let angle = ((posXinit - posXfinal) * -1) / 10;

                if (model !== undefined) {
                    model.setLinearVelocity(new THREE.Vector3(angle, velocite,-velocite/1.5));
                    model.setAngularVelocity(new THREE.Vector3(angle, angle, angle));

                    setTimeout(() => {
                        model.__dirtyPosition = false;
                        model.position.set(posBallonInitX, posBallonInitY, posBallonInitZ);
                        ballonPris = false;
                        ballonLancer = false;
                        model.__dirtyPosition = true;
                        peutCompter = true;

                    }, 5000);
                }
            }
        }
    }

    detecterCollisionPoints() {

        if (!gameover) {
            if (model.position.x >= colRimGauche.position.x &&
                model.position.z <= colRimCentre.position.z &&
                model.position.x <= colRimDroite.position.x &&
                model.position.y < colRimCentre.position.y &&
                model.position.y > colRimCentre.position.y - 1 &&
                model.position.z >= colPanneau.position.z && peutCompter) {

                pointage++;
                peutCompter = false;

                createjs.Sound.play('goal');

                document.querySelector('.highscore-espace').innerHTML = this.nomJoueur+ " : " + pointage;
            }
        }
    }

    detecterCollisionObjets() {

        if (peutJouerSon) {

            createjs.Sound.play('bounce');
            peutJouerSon = false;
            setTimeout(() => peutJouerSon = true, 1000);

        }
    }

    detruire() {
        gameover = true;
        createjs.Sound.stop();

        document.getElementById('timer').style.display = 'none';
        timer = 60;
        $('#timer').text(timer);

        clearInterval(interval);
        interval = null;

        this.video.style.display = 'none';
        this.video.pause();

        createjs.Ticker.removeEventListener('tick', this.ecouteur.ecouteurTicker);
        model.removeEventListener('collision', this.detecterCollisionObjets.bind(this));
        model.removeEventListener('lancer', this.lancerBallon.bind(this));

        document.querySelector('#conteneur-espace').style.display = "none";

        if(scene !== null){
            for (let i = scene.children.length - 1; i >= 0; i--) {
                let obj = scene.children[i];
                scene.remove(obj);

                if (scene.children.length === 0) {

                    document.body.removeChild(renderer.domElement);
                    document.body.removeChild(render_stats.domElement);
                    document.body.removeChild(physics_stats.domElement);

                    render_stats = null;
                    physics_stats = null;
                    loader = null;

                    scene = null;
                    renderer = null;
                    camera = null;

                }
            }
        }
    }

    animate() {

        if (!gameover) {
            if (model !== undefined) {
                if (ballonPris !== true) {

                    model.__dirtyPosition = true;
                    model.setLinearVelocity(new THREE.Vector3(0, 0, 0));
                    model.setAngularVelocity(new THREE.Vector3(0, 0, 0));
                }
            }

            if (panierEspace !== undefined) {

                panierEspace.scale.set(4, 4, 4);
                panierEspace.position.z = -30;
                panierEspace.position.y = -11.5;

            }
            scene.simulate();
            renderer.render(scene, camera);
            requestAnimationFrame(this.animate.bind(this));
            render_stats.update();
            physics_stats.update();
        }
    }
}
