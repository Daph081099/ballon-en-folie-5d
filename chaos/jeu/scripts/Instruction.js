export class Instruction extends createjs.Bitmap{
    constructor(atlas){
        super(atlas);

        this.alpha = 0;
    }

    apparaitre(){

        createjs.Tween
            .get(this)
            .to({alpha:1}, 500, createjs.Ease.linear);
    }

    enlever(){
        createjs.Tween
            .get(this)
            .to({alpha:0}, 500, createjs.Ease.linear)
    }
}